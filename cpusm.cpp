#include "./cpusm.h"
char *p, *outputfile, *linebacker, *uname, *freqgov, *appstart, *dev;
char path[255], ty_string[40], xy_string[40], sysid[255], minfreq[16], maxfreq[16], cpus[19], version[47], disk[8][255], model[2][255], cpuol[3], proccmd[104],threadcmd[109], status[2][1024], prun[2][1024];
int redis0, xcnt, y, samples, cr, sig, mcnt = 0, diskcnt, rate, diskstats[8][7], diskstats0[8][7], memstat[5], cpustat[8], cpustat0[8], deltaStat[8], deltaStatv[8][8], freqcpus[8], cpustatv[8][8], cpustatv0[8][8];
int redis, flip, qual, netdis, cpuspinm, colour, prec, cntsumr, slat, slot, idurm, idurh, idurd, idurms, idurhs, idurds, threads, procs, procsrun, actlog, procproc, scroll, secs, rcnt, ncnt, acnt, corecnt, verb;
double calce, calce0, calcdur, calcdur0, calcsps, rvar, cnt, cntf, cnt0, cnt1, ctemp, ftemp, decsecs, iLoad, cLoad, cLoadv[8], iLoadv, tcLoadv, tcLoad, uLoad, nLoad, sLoad, wLoad, qLoad, rLoad, irqLoad, acLoad;
double rxbits, rxpp, rxpps, deltarxbits, deltarxpp, rxbitspast, rxpppast, txbits, txpp, txpps, deltatxbits, deltatxpp, txbitspast, txpppast, rxMbps, txMbps, rxMbpssum, rxMbpsavg, txMbpssum, txMbpsavg;
double pcpu, cpuspins, cpuspin, ttstart, idurs, idurss, idur, iduri, idur1, idurv, cpurollA, rxrollA, txrollA, rxprollA, txprollA, freqs[8], freq, loadavg[3];
double netstat[10], diskstatR, diskstatRsum, diskstatRa, diskstatWsum, diskstatWa, diskstatW, deltadiskR[8], deltadiskW[8], cntsum, secdecs, decvar;
double cpuroll[60000], rxroll[60000], txroll[60000], diskrollR[60000], diskrollW[60000], diskrollRa, diskrollWa;
FILE *fp, *fo, *fpp, *mp;
size_t yards=247;
clock_t exetime = 0, exetime0 = 0;
typedef unsigned long long cnt_nsec_t;
cnt_nsec_t exptime, cnttime;

int rng(const int nMin, const int nMax, const int  nN);
void zeroCounters(void);
void print_stats(void);
void print_app_usage(void);
void print_header(void);
void print_logheader(void);
void print_help(void);
void time_diff(void);
void *slapshot(void *);
void *dmtp(void);
static cnt_nsec_t get_time_nsec(void);
static void old_attr(void);
void print_current_time_with_ms(void);
double derive_interval(void);
void check_60(double zecz);

struct timespec pseudoSleep = {0,0}, tstart= {0,0}, tend= {0,0};
struct timespec ;
static struct termios g_old_kbd_mode; 

static void old_attr(void){ 
	tcsetattr(0, TCSANOW, &g_old_kbd_mode); 
	return;
}

static cnt_nsec_t get_time_nsec(void)
{
	clock_gettime(CLOCK_MONOTONIC, &tstart);
	return NSECS *(double)tstart.tv_sec + tstart.tv_nsec;
}

void sig_handler(int signo)
{
	if (signo == SIGINT) {
		sig = SIGINT;
		time_diff();
	} else if (signo == SIGTRAP) {
		sig = SIGTRAP;
		time_diff();
	} else if (signo == SIGQUIT) {
		sig = SIGQUIT;
		time_diff();
		exit(0);
	} else {
		time_diff();
	}
	return;
}
int rng(const int nMin, const int nMax, const int  nN)
{
  int nR = 0;
  for (int i = 0; i < nN; i++)
  {
    nR = rand()%(nMax-nMin) + nMin;
  }
  return nR;
}
void print_current_time_with_ms (void)
{
	long  ms;
	time_t s;
	struct timespec spec;
	struct tm* myty;
	char tyu_string[40];
	clock_gettime(CLOCK_REALTIME, &spec);
	s  = spec.tv_sec;
	ms = round(spec.tv_nsec / 1.0e7);
	if (ms > 99) {
		s++;
		ms = 0;
	}
	myty = localtime (&spec.tv_sec);
	if (rcnt == 0 ) {
		strftime (xy_string, sizeof (xy_string), "%H:%M:%S", myty);
		sprintf(tyu_string, ".%02ld", ms);
		strcat(xy_string, tyu_string);
	}
	strftime (ty_string, sizeof (ty_string), "%H:%M:%S", myty);
	sprintf(tyu_string, ".%02ld", ms);
	strcat(ty_string, tyu_string);
	
	return;
}
void check_60(double zecz)
{
	char secheck[7];
	if ( secdecs >= 0.1 ) sprintf(secheck, "%05.2f", zecz);
	if ( secdecs < 0.1 ) sprintf(secheck, "%06.3f", zecz);
	int zeczint = atoi(strtok(secheck,"."));
	if ( zeczint == 60 ) {
		idurm++;
		if ( idurm == 60 ) {
			idurh++;
			idurm = 0;
		}
		idurs = 0;
	}
	return;
}
void time_diff(void)
{
	clock_gettime(CLOCK_MONOTONIC, &tend);
	double diff_time = ((double)tend.tv_sec + 1.0e-9*tend.tv_nsec) - ttstart - (double)rate*(double)slat*decsecs*HSLEEP;
	double x_samples=samples!=0?(acnt>=samples?samples:idur/(secdecs)):((idurv==0?rint(diff_time/(secdecs)):(rcnt>idurv/(secdecs)?rint(((idurv-idur)/(secdecs))):rint((idurv-idur)/(secdecs)))));
	double run_time = diff_time; 
	idurd = (int)diff_time/86400;
	diff_time = fmod(diff_time, 86400);
	idurh = (int)diff_time/3600;
	diff_time = fmod(diff_time, 3600);
	idurm = (int)diff_time/60;
	idurs = fmod(diff_time, 60);
	if ( idurs >= 59.9 && scroll != 2 ) check_60(idurs);
	double a_samples = rcnt;
	rvar = (a_samples-x_samples)/x_samples;
	if (sig == SIGINT || sig == SIGQUIT ) {
		if ( colour != 0 ) printf (LEFTSTATS);
		printf ("\n\n start.time:\t\t%s\n", xy_string);
		print_current_time_with_ms();
		exetime = clock();
		cpuspin = (double)( exetime - exetime0 ) / (double)(CLOCKS_PER_SEC);
		cpuspinm = cpuspin/60;
		cpuspins = fmod(cpuspin,60);
		pcpu = cpuspin / cntsum * 100 / corecnt;
		if (sig == SIGQUIT) printf (" end.time:\t\t%s\n", ty_string);
		if (sig == SIGINT) printf (" int.time:\t\t%s\n", ty_string);
		if ( idurd == 0 ) printf(" run.time:\t\t%02i:%02i:%05.2f\n", idurh,idurm,idurs);
		if ( idurd != 0 ) printf(" run.time:\t\t%02id:%02i:%02i:%05.2f\n", idurd, idurh,idurm,idurs);
		printf(" cpu.time:\t\t%i:%05.2f\n", cpuspinm, cpuspins);
		printf(" %1scpu:\t\t\t%-4.1f\n", "%", pcpu);
		if (sig == SIGQUIT) printf(" actual.samples/s:\t%-5.2f\n", a_samples/run_time);
		if (sig == SIGINT) printf(" int.samples/s:\t\t%-5.2f\n", a_samples/run_time);
		printf(" exp.samples/s:\t\t%-5.2f\n", (double)1/(secdecs));	
		if (sig == SIGQUIT) printf(" actual.samples:\t%-9.0f \n", a_samples);
		if (sig == SIGINT) printf(" int.samples:\t\t%-9.0f \n", a_samples);
		printf(" exp.samples:\t\t%-9.0f \n", x_samples);
		printf(" nominal.variation:\t%-9.2f\n", a_samples-x_samples);
		printf(" relative.variation:\t%-9.4f\n\n\n", rvar);
		if ( colour != 0 ) printf(ILOADLOW);
		if ( sig != SIGQUIT ) {
			print_header();
			return;   
		}
		if (sig == SIGQUIT && actlog >= 1) {
			fprintf (fo, "\n\n start.time:\t\t%s\n", xy_string);
			fprintf (fo, " end.time:\t\t%s\n", ty_string);
			if ( idurd == 0 ) fprintf(fo, " run.time:\t\t%02i:%02i:%05.2f\n", idurh,idurm,idurs);
			if ( idurd != 0 ) fprintf(fo, " run.time:\t\t%02id:%02i:%02i:%05.2f\n", idurd, idurh,idurm,idurs);
			fprintf (fo, " cpu.time:\t\t%i:%05.2f\n", cpuspinm, cpuspins);
			fprintf (fo, " %1scpu:\t\t\t%-4.1f\n", "%", pcpu);
			fprintf (fo, " actual.samples/s:\t%-5.2f\n", a_samples/run_time);
			fprintf (fo, " exp.samples/s:\t\t%-5.2f\n", (double)1/(secdecs));
			fprintf (fo, " actual.samples:\t%-9.2f \n", a_samples);
			fprintf (fo, " exp.samples:\t\t%-9.2f \n", x_samples);
			fprintf (fo, " nominal.variation:\t%-9.2f\n", a_samples-x_samples);
			fprintf (fo, " relative.variation:\t%-9.4f\n\n\n", rvar);
		}
	} else if ( sig == SIGTRAP && actlog <= 1 ) {
		if ( colour != 0 ) printf(HEADER);
		printf ("\ncpu statistics monitor________________________________________________________________________________________________________________________________________________________\n\n");
		if ( colour != 0 ) printf(ILOADLOW);
		printf ("  system.type:\t %s\n", sysid);
		printf ("  hostname:\t %s\n", uname);
		printf ("  o/s.version:\t %s\n", version);
		printf ("  cpu.model:\t%s", model[1]);
		printf ("  cpu.cores:\t %i [%s]\n", corecnt, cpuol);
		printf ("  min.freq:\t %.2f MHz\n  max.freq:\t %.2f MHz\n  governor:\t %9s", atof(minfreq), atof(maxfreq), freqgov);
		printf ("  sys.memory:\t %i kB\n", memstat[0]);
		printf ("  sys.disks:\t %i ", diskcnt);
		for (int i=0;i<diskcnt;i++) {
			printf ("[%s]", disk[i]);
		}
		printf ("\n");
		if ( netdis != 0 ) printf ("  net.device:\t %s \n", dev);
		print_current_time_with_ms();
		printf ("  start time:\t %s\n", xy_string);
		printf ("  current time:\t %s\n", ty_string);
		if ( idurd == 0 ) printf ("  int.run.time:\t %02i:%02i:%05.2f\n", idurh,idurm,idurs);
		if ( idurd != 0 ) printf ("  int.run.time:\t %02id:%02i:%02i:%05.2f\n", idurd,idurh,idurm,idurs);
		if ( idurd == 0 ) printf ("  exp.run.time:\t %02i:%02i:%05.2f\n", idurhs,idurms,idurss);
		if ( idurd != 0 ) printf ("  exp.run.time:\t %02id:%02i:%02i:%05.2f\n", idurds,idurhs,idurms,idurss);
		printf ("  interval:\t %-7.3f\n", secdecs);
		printf ("  int.samples/s: %-5.2f\n", a_samples/run_time);
		printf ("  exp.samples/s: %-5.2f\n", (double)1/(secdecs));
		printf ("  int.samples:\t %.2f\n", a_samples);	
		printf ("  exp.samples:\t %.2f\n", samples!=0?samples:rint(idurv/(secdecs)));
		if (actlog >= 1) printf ("  logging to:\t %s\n", outputfile);
		printf ("\n");
		if ( colour != 0 ) printf(LEFTSTATS);
		printf("  RUNTIME OPTIONS\n");
		printf("  <h>: show runtime help\n");
		printf("  <c>: switch stdout colour\n");
		printf("  <q>: quit cpusm\n");
		printf("  <s>: display interim cpusm summary statistics \n");
		if ( redis == 0 ) printf("  <x>: enable periodic header printing \n");
		if ( redis != 0 ) printf("  <x>: disable periodic header printing \n");
		printf("  <z>: restart cpusm\n");
		printf("  </>: switch output mode [fixed-line/scrolling]\n");	
		printf("  <.>: display column headings\n");
		if ( actlog >= 1 ) printf("  <,>: enable/diable stdout [file logging-mode]]\n");
		printf("  <SPACE>: switch display mode [extended-cpu/virtual-cpu/network-interface]\n\n");
		if ( colour != 0 ) printf(ILOADHI);	    
		printf ("  %s\n\n", appstart);
		for (int i=HSLEEP; i>0; i--) {
			usleep(rate*secdecs*USECS);
			printf("  active monitoring resumes in %02i seconds\n", i-1);
			printf ("\x1b[1A");
		}
		slot = 19;
		slat++;
		print_header();
	}
	return;
}
void *slapshot(void *)
{
	if( access(DSTAT, F_OK ) != -1 ) {
		fp = fopen(DSTAT, "r");
		if (fp != NULL) {
			while (getline (&linebacker, &yards, fp) != -1) {
				for ( int j=0;j<diskcnt;j++ ) {
					if (strstr(linebacker, disk[j]) != NULL) {
						strtok(linebacker, " ");
						strtok(NULL," ");
						strtok(NULL," ");
						for (int i=0; i<7; i++) {
							diskstats[j][i] = atoi(strtok(NULL," "));
						}
					}
				}
			}
		}
		fclose(fp);
	}
	if( access(CPUI, F_OK ) != -1 ) {
		fp = fopen(CPUI, "r");
		if (fp != NULL) {
			freq = 0;
			for(int i=0;i<corecnt;i++) {
				while (getline (&linebacker, &yards, fp) != -1) {
					char *fr = strtok(linebacker, "\t");
					 if (strcmp(fr, "cpu MHz") == 0) {
						freqs[i] = atof(strtok(NULL, ":\t"));
						break;
					}
				}
				freq += freqs[i];
			}
			freq /= corecnt;
		}
		fclose(fp);
	}
	if( access(SOCT, F_OK ) != -1 ) {
		fp = fopen(SOCT, "r");
		if (fp != NULL) {

			getline (&linebacker, &yards, fp);
			ctemp = atoi(linebacker);
		}
		fclose(fp);
	}
	ctemp /= (double)1000;
	ftemp = ctemp*9/5+32;
	if( access(LDAVG, F_OK ) != -1 ) {
		fp = fopen(LDAVG, "r");
		if (fp != NULL) {
			getline (&linebacker, &yards, fp);
			loadavg[0] = atof((strtok(linebacker," ")));
			for (int i=1; i<3; i++) {
				loadavg[i] = atof((strtok(NULL," ")));
			}
		}
		fclose(fp);
	}
	if( access(PSTAT, F_OK ) != -1 ) {
		fp = fopen(PSTAT, "r");
		if (fp != NULL) {
			getline (&linebacker, &yards, fp);
				int a = 0;
				while (getline (&linebacker, &yards, fp) != -1) {
					sprintf(prun[0], "%s", (strtok(linebacker, "01234567")));
					if (strcmp(prun[0], "cpu") == 0) {
						for (int i=0; i<7; i++) {
							cpustatv[a][i] = atoi((strtok(NULL," ")));
							cpustat[i] += cpustatv[a][i];
						}
					} else {
						break;
					}
					a++;
				}
			if ( cntsumr % VARINT != 0 ) flip = 0;
			if ( ENAINT != 0 || (cntsumr % VARINT == 0 && flip == 0) ) {
				flip = 1;
				while (getline (&linebacker, &yards, fp) != -1) {
					sprintf(prun[0], "%s", (strtok(linebacker, " ")));
					sprintf(prun[1], "%s", (strtok(NULL, "\n ")));
					if (strcmp(prun[0], "procs_running") == 0) {
						break;
					}
				}
				procsrun = atoi(prun[1]);
			}
		}
		fclose(fp);
	}
	if (procproc == 0) mcnt++;
	if ( ENAINT != 0 || (cntsumr % VARINT == 0 && flip == 1 )) {
		flip = 2;
		if(procproc == 1) {
			DIR *d;
			struct dirent *dir;
			threads = 0;
			procs = 0;
			mcnt++;
			d = opendir("/proc/");
			if (d) {
				while ((dir = readdir(d)) != NULL) {
					char path[255] = {};
					strcat(path, "/proc/");
					strcat(path, dir->d_name);
					strcat(path, "/status");
					if( access(path, F_OK ) != -1 ) {
						fpp = fopen(path, "r");
						if (fpp != NULL) {
							while (getline (&linebacker, &yards, fpp) != -1) {
								sprintf(status[0], "%s", (strtok(linebacker, ":")));
								sprintf(status[1], "%s", (strtok(NULL, ": ")));
								if (strcmp(status[0], "Threads") == 0) {
									break;
								}
							}
							threads += atoi(status[1]);
							procs++;
						}
						fclose(fpp);
					}
				}
				closedir(d);
			}
		}
		if(procproc == 2) {
			mcnt++;
			fpp = popen(threadcmd, "r");
			getline (&linebacker, &yards, fpp);
			threads = atoi(linebacker);
			pclose(fpp);
			fpp = popen(proccmd, "r");
			getline (&linebacker, &yards, fpp);
			procs = atoi(linebacker);
			pclose(fpp);
		}
		if( access(MSTAT, F_OK ) != -1 ) {
			mp = fopen(MSTAT, "r");
			if (mp != NULL) {
				for(int mm=0; mm<3; mm++) {
					getline(&linebacker, &yards, mp);
					strtok(linebacker," ");
					memstat[mm] = atoi(strtok(NULL, " "));
				}
				while (getline (&linebacker, &yards, mp) != -1) {
					char *fr = strtok(linebacker," ");
					if (strcmp(fr, "SwapTotal:") == 0) {
						memstat[3] = atoi(strtok(NULL, " "));
					}
					if (strcmp(fr, "SwapFree:") == 0) {
						memstat[4] = atoi(strtok(NULL, " "));
						break;
					}
				}
			}
			if ( scroll == 48 ) printf ("swaptotal: %i free: %i\n", memstat[3], memstat[4]);
			fclose(mp);
		}
	}
	if ( netdis != 0 ) {
		if( access(NETSTAT, F_OK ) != -1 ) {
			fpp = fopen(NETSTAT, "r");
			while (getline (&linebacker, &yards, fpp) != -1) {
				if (strcmp(strtok(linebacker, " :"), dev) == 0) {
					for (int i=0;i<10;i++) {
						netstat[i] = atof(strtok(NULL, " "));
					}
					break;
				}
			}
			fclose(fpp);
		}
	}
	pthread_exit(NULL);
	return 0;
}
int main(int argc, char **argv)
{
	exetime0 = clock();
	dev = (char *) malloc (yards + 1);
	char netdevcmd[75];
	strcpy(netdevcmd, DEFDEV);
	fp = popen(netdevcmd, "r");
	if ( getline (&linebacker, &yards, fp) != -1 ) {;
		sprintf(dev, "%s", (strtok(linebacker, " \n")));
		netdis = 2;
	}
	pclose(fp);
	int norate = 0;
	zeroCounters();
	pid_t ppid;
	ppid = getpid();
	fp = fopen(PPMYD, "w");
	fprintf (fp, "%i\n", ppid);
	fclose(fp);
	int op;
	colour = 1;
	idur = 0;
	procproc = 1;
	actlog = 0;
	prec = 0;
	scroll = 0;
	verb = 2;
	opterr = 0;
	qual = 0;
	redis=redis0=0;
	y = 0;
	while ((op = getopt (argc, argv, "c:d:f:hk:l:n:p:q:r:s:t:v:x:z:")) != -1) {
		switch (op)
		{
			case 'c':
				colour = atoi(optarg);
				if ( colour >= 3 ) {
					print_app_usage();
					exit(EXIT_FAILURE);
				}				
				break;
			case 'd':
				idur = idurv = atof(optarg);
				break;
			case 'f':
				outputfile = optarg;
				fo = fopen(outputfile, "w+");
				if( fo == NULL ) {
					fprintf ( stderr, "execution error [log file inaccessible]: -f %s\n\n", outputfile);
					exit(EXIT_FAILURE);
				} 
				break;
			case 'h':
				print_app_usage();
				exit(EXIT_FAILURE);
				break;
			case 'k':
				procproc = atoi(optarg);
				if ( procproc >= 3 ) {
					print_app_usage();
					exit(EXIT_FAILURE);
				}				
				break;
			case 'l':
				actlog = atoi(optarg);
				if ( actlog >= 3 ) {
					print_app_usage();
					exit(EXIT_FAILURE);
				}
				break;
			case 'n':
				sprintf(dev, "%s", optarg);
				netdis = 1;
				break;
			case 'p':
				prec = atoi(optarg);
				if ( prec >= 2 ) {
					print_app_usage();
					exit(EXIT_FAILURE);
				}				
				break;
			case 'q':
				qual = atoi(optarg);
				if ( qual >= 2 ) {
					print_app_usage();
					exit(EXIT_FAILURE);
				}				
				break;
			case 'r':
				rate = atoi(optarg);
				if ( rate > RATEMAX || rate < 1 ) {
					fprintf ( stderr, "execution error [sample rate violation]: -r %s\n", optarg);
					fprintf ( stderr, "sample rate limit: 1 <= r <= %i\n", RATEMAX);
					exit(EXIT_FAILURE);
				}
				break;
			case 's':
				scroll = atoi(optarg);
				break;
			case 't':
				secs = atoi(optarg);
				decsecs = fmod(atof(optarg), 1);
				if ( (double)secs+decsecs < INTMIN ) {
					fprintf ( stderr, "execution error [interval violation]: -t %s\n", optarg);
					fprintf ( stderr, "interval limit: t >= %6.3f\n", INTMIN);
					exit(EXIT_FAILURE);
				}
				break;
			case 'x':
				redis = atoi(optarg);
				redis0 = redis;
				break;
			case 'z':
				samples = atoi(optarg);
				break;
			case 'v':
				verb = atoi(optarg);
				if ( verb >= 4 ) {
					print_app_usage();
					exit(EXIT_FAILURE);
				}				
				break;
			case '?':
				if ( optopt == 'c' || optopt == 'd' || optopt == 'f' || optopt == 'k' || optopt == 'l' || optopt == 'n' || optopt == 'q'  || optopt == 'p' || optopt == 's' || optopt == 't' || optopt == 'v' || optopt == 'x' || optopt == 'z' ) {
					fprintf ( stderr, "execution error [illegal command-line option value]: -%c\n", optopt);
					print_app_usage();
					exit(EXIT_FAILURE);
				} else {
					fprintf ( stderr, "execution error [illegal command-line option]: -%c\n", optopt);
					print_app_usage();
					exit(EXIT_FAILURE);
				} 
				break;
			default:
				abort ();
		}
	}
	for (int i=optind;i<argc;i++) {
		printf ("execution error [unknown command-line option]: %s\n\n", argv[i]);
		exit(0);
	}
	if ( actlog >= 1 && !outputfile ) {
		printf ("execution error - log file unspecified [-f 'filename']\n\n");
		exit(0);
	}
	if ( netdis == 0 ) {
		printf ("execution error - network interface unspecified [-n 'device']\n\n");
		exit(0);
	}
	if ( netdis == 2 ) printf("network interface unspecified - using device bound to default route: -n %s\n", dev);
	if (secs+decsecs == 0) {
		if ( rate == 0 ) {
			printf ("sample interval/rate unspecified - assuming default: -t 0.1\n");
			decsecs = 0.1;
			secs = 0;
			norate = 2;
		}else {
			if ( rate == 1 ) {
				decsecs = 0;
				secs = 1;
			}else if ( rate <= 200 ) {
				decsecs = 1/(double)rate;
				secs = 0;
			}else if ( rate >= 201 ) {
				printf ("execution error [sample rate violation]: %i\n\n", rate);
				print_app_usage();
				exit(EXIT_FAILURE);
			}
			norate = 1;
		}
	} else if ((secs+decsecs) < 0.005) {
		printf ("execution error [sample interval violation]: %f\n\n", secs+decsecs);
		print_app_usage();
		exit(EXIT_FAILURE);
	} else {
		if (rate != 0 && rate != (double)1/(secs+decsecs) ) {
			printf ("option conflit: -t%.3f -r%i\n\n", secs+decsecs, rate);
			print_app_usage();
			exit(EXIT_FAILURE);
		}
		norate = 2;
	}
	if (samples != 0 && idurv !=0 ) {
		printf ("option conflit: -d%.2f -z%i\n\n", idurv, samples);
		print_app_usage();
		exit(EXIT_FAILURE);
	}
	secdecs = (double)secs+decsecs;
	rate=rate!=0?rate:rint((double)1/secdecs);
	appstart = (char *) malloc (yards + 1);
	char *appstart0 = (char *) malloc (yards + 1);
	sprintf( appstart, "./cpusm -c%i -d%.2f -k%i -l%i", colour, idurv, procproc, actlog);
	if ( actlog != 0 ) strcat( appstart, " -f");
	if ( actlog != 0 ) strcat( appstart, outputfile);
	if ( norate == 2 ) {
		if ( netdis != 0 ) {
			sprintf( appstart0, " -n %s -p%i -q%i -s%i -t%.3f -v%i -z%i", dev, prec, qual, scroll, secdecs, verb, samples);
		} else {
			sprintf( appstart0, " -p%i -q%i -s%i -t%.3f -v%i -z%i", prec, qual, scroll, secdecs, verb, samples);
		}
	} else if ( norate == 1 ) {
		if ( netdis != 0 ) {
			sprintf( appstart0, " -n %s -p%i -q%i -r%i -s%i -v%i -z%i", dev, prec, qual, rate, scroll, verb, samples);
		} else {
			sprintf( appstart0, " -p%i -q%i -r%i -s%i -v%i -z%i", prec, qual, rate, scroll, verb, samples);
		}
	}
	strcat(appstart, appstart0);    
	if ( idur != 0 || samples !=0 ) {
		iduri=samples!= 0?samples*secdecs:idur;
		idurds = iduri/86400;
		iduri = fmod(iduri, 86400);
		idurhs = iduri/3600;
		iduri = fmod(iduri, 3600);
		idurms = iduri/60;
		idurss = fmod(iduri, 60);
	}
	char freqcmd[128];
	strcpy(freqcmd, FREQ);
	fp = popen(freqcmd, "r");
	getline (&linebacker, &yards, fp);
	pclose(fp);
	sprintf(minfreq, "%s", (strtok(linebacker, ",")));
	sprintf(maxfreq, "%s", (strtok(NULL, "\n")));
	char sysidcmd[9];
	strcpy(sysidcmd, ID);
	fp = popen(sysidcmd, "r");
	getline (&linebacker, &yards, fp);
	pclose(fp);
	if (strcmp(strtok(linebacker, " \n"), "Linux") == 0) {
		strcpy(sysid, "linux");
	}
	if (strcmp(strtok(linebacker, " \n"), "Darwin") == 0) {
		strcpy(sysid, "mac-os");
	}
	if( access(MSTAT, F_OK ) != -1 ) {
		mp = fopen(MSTAT, "r");
		if (mp != NULL) {
			for(int mm=0; mm<3; mm++) {
				getline(&linebacker, &yards, mp);
				strtok(linebacker," ");
				memstat[mm] = atoi(strtok(NULL, " "));
			}
		}
		fclose(mp);
	}
	char hddcmd[61];
	strcpy(hddcmd, DEFDSK);
	fp = popen(hddcmd, "r");
	if ( fp != NULL ) {
		int a = 0;
		while (getline (&linebacker, &yards, fp) != -1) {
			sprintf(disk[a], "%s", strtok(linebacker, " \n"));
			a++;
		}
		diskcnt = a;
	}
	fclose(fp);
	printf("\npid: %i\n", ppid);
	signal(SIGINT, sig_handler);
	dmtp();
	if ( y == 11 ) {
		old_attr();
		execv(argv[0], argv);
	}
	time_diff();
	return 0;
}
double derive_interval(void)
{
	if ( qual == 0 ) {
		cnttime = get_time_nsec();
		if ( slot == 19 ) {
			exptime += (rate*secdecs*NSECS*HSLEEP);
			exptime += NSECS * secdecs;
			slot = 1;
		} else {
			exptime += NSECS * secdecs;
		}
		if (cnttime < exptime && slot != 2) {
			decvar = (exptime - cnttime);
			decvar /= NSECS;
			slot=slot==4?5:4;
		} else if (cnttime > exptime && slot != 4) {
			if ( cnttime - exptime <= secdecs  || slot ==1 ) {
				decvar = cnttime - exptime;
				decvar /= NSECS;
				slot=slot==2?3:2;
			}
		} else {
			decvar = secdecs;
			slot=7;
		}
	} else {
		decvar = secdecs;
	}
	return decvar;
}
void *dmtp(void)
{
	char ch; 
	int tt = 0;
	int nt = 0;
	int ntc = 0;
	int x = 0;
	int scsw = 0;
	pthread_t tsid[1];
	struct termios new_kbd_mode;

	strcpy(threadcmd, THREADS);
	strcpy(proccmd, PROCS);
	if( access(AFFCPU, F_OK ) != -1 ) {
		fp = fopen(AFFCPU, "r");
		if (fp != NULL) {
			getline (&linebacker, &yards, fp);
			sprintf(cpuol, "%s", (strtok(linebacker, "\n")));
		}
		fclose(fp);
	}
	if( access(CPUI, F_OK ) != -1 ) {
		fp = fopen(CPUI, "r");
		if (fp != NULL) {
			while (getline (&linebacker, &yards, fp) != -1) {
				sprintf(model[0], "%s", (strtok(linebacker, ":")));
				sprintf(model[1], "%s", (strtok(NULL, ":")));
				if (strcmp(model[0], "model name\t") == 0) {
					break;
				}
			}
		}
		fclose(fp);
	}
	if( access(CPUI, F_OK ) != -1 ) {
	fp = fopen(CPUI, "r");
	if (fp != NULL) { 
			while (getline (&linebacker, &yards, fp) != -1) {
				char *fr = strtok(linebacker, "\t");
				 if (strcmp(fr, "siblings") == 0) {
					corecnt = atoi(strtok(NULL, ":\t"));
					break;
				}
			}
	}
	fclose(fp);
	}
	if( access(VER, F_OK ) != -1 ) {
		fp = fopen(VER, "r");
		if (fp != NULL) {
			getline (&linebacker, &yards, fp);
			sprintf(version, "%s", (strtok(linebacker, " ")));
			for (int i=0; i<2; i++) {
				sprintf(version, "%s", (strtok(NULL, " ")));
			}
		}
		fclose(fp);
	}
	if( access(HN, F_OK ) != -1 ) {
		fp = fopen(HN, "r");
		if (fp != NULL) {
			uname = (char *) malloc (yards + 1);
			getline (&linebacker, &yards, fp);
			sprintf(uname, "%s", (strtok(linebacker, "\n")));
		}
		fclose(fp);
	}
	if( access(GOV, F_OK ) != -1 ) {
		fp = fopen(GOV, "r");
		if (fp != NULL) {
			freqgov = (char *) malloc (yards + 1);
			getline (&freqgov, &yards, fp);
		}
		fclose(fp);
	}
	tcgetattr(0, &g_old_kbd_mode); 
	memcpy(&new_kbd_mode, &g_old_kbd_mode, sizeof(struct termios)); 
	new_kbd_mode.c_lflag &= ~(ICANON | ECHO); 
	new_kbd_mode.c_cc[VTIME] = 0; 
	new_kbd_mode.c_cc[VMIN] = 1; 
	tcsetattr(0, TCSANOW, &new_kbd_mode); 
	atexit(old_attr);
	exptime = get_time_nsec();
	while ((samples!=0)?(rcnt<samples):((idurv!=0)?(rcnt<idurv/(secdecs)):(1))) {
		if ( rcnt == 0 ) print_help();
		if ( rcnt == 0 ) {
			clock_gettime(CLOCK_MONOTONIC, &tstart);
			ttstart = (double)tstart.tv_sec + NSECS2SECS*tstart.tv_nsec;
			cnt1 = ((double)tstart.tv_sec + NSECS2SECS*tstart.tv_nsec);
		}
		linebacker = (char *) malloc (yards + 1);
		pthread_create(&tsid[0], NULL, &slapshot, NULL);
		fd_set readfds;
		int fd_stdin;
		fd_stdin = fileno(stdin);
		FD_ZERO(&readfds);
		FD_SET(fileno(stdin), &readfds);
		fflush(stdin);
		fflush(stdout);
		derive_interval();
		pseudoSleep.tv_sec = decvar/1;
		pseudoSleep.tv_nsec = fmod(decvar, 1) * NSECS;	
		if ( ntc != 0 ) ntc--;
		int sret = pselect(fd_stdin + 1, &readfds, NULL, NULL, &pseudoSleep, NULL);
		if ( sret > 0 ) {	
			ch = getchar();  
			if (scroll == 47) {
				printf("caught: %d\n\n", ch);
			}
			if ( ch == '/' ) {
				scroll = (scroll==0)?(1):(0);
				if ( scroll == 1 && redis == 0 ) x=10;
				if ( scroll == 1 && redis != 0 ) xcnt = 0;
				if ( scroll == 0 && redis == 0 ) scsw=1;
				if ( scroll == 0 && redis != 0 ) print_header();
			} 
			if ( ch == ',' && actlog >=1 ) {
				actlog = (actlog==1)?(2):(1);
				if ( actlog == 1 && scroll == 1) print_header();
			} 
			if ( ch == '.' ) {
				if ( rcnt != 0 && scroll == 0 && scsw == 0 ) {
					printf("\x1b[7A");
				}
				print_header();
				if ( scsw == 1 ) scsw = 0;
			}
			if ( ch == 'm' || ch == ' ' ) {
				if ( netdis != 0 ) verb = verb==0?1:verb==1?2:verb==2?3:0;
				if ( netdis == 0 ) verb = verb==0?2:verb==2?3:0;
				if ( rcnt != 0 && scroll == 0 && scsw == 0 ) {
					printf("\x1b[7A");
				}
				print_header();
				if ( scsw == 1 ) scsw = 0;
			}
			if ( ch == 'q' ) {
				sig_handler(SIGQUIT);
			}
			if ( ch == 'x' ) {
				redis=redis==0?redis0!=0?redis0:40:0;
				if ( redis != 0 && scroll != 0 ) print_header();
			}
			if ( ch == 's' ) {
				sig_handler(SIGINT);
			}
			if ( ch == '!' ) {
				scroll = scroll==13?1:13;
			}
			if ( ch == 'c' ) {
				colour = colour==0?1:colour==1?2:0;
			}
			if ( ch == 'z' ) {
				y = 11;
				return 0;
			}
			if ( ch == 'h' && scroll != 0 ) {
				sig_handler(SIGTRAP);
			}
		}
		if (scroll == 0 || ( scroll == 1 && x == 10)) {
			if(actlog <= 1) {
				printf ("\x1b[1A");
			}
			x=0;
		}
		pthread_join(tsid[0], NULL);
		clock_gettime(CLOCK_MONOTONIC, &tstart);
		cnt0 = ((double)tstart.tv_sec + NSECS2SECS*tstart.tv_nsec);
		cntf = cnt;
		cnt = cnt0 - cnt1;
		if ( slot == 19 ) cnt -= rate*secdecs*HSLEEP;
		cntsum += cnt;
		cntsumr = rint(cntsum);
		if (rcnt == 0 ) cntf = cnt;
		cnt1 = cnt0;
		if ( prec == 0 ) idur1 = cnt;
		if ( prec == 1 ) idur1 = secdecs;
		if ( idurv !=0 ) idur -= idur1;
		if ( idurv ==0 ) idur += idur1;
		iduri = idur;
		idurh = (int)iduri/3600;
		iduri = fmod(iduri, 3600);
		idurm = (int)(iduri/60);
		idurs = fmod(iduri, 60);
		if ( idurs >= -0.005 && idurs <= 0 ) idurs = 0;
		for (int i=0; i<7; i++) {
			if ( i == 0 ) deltaStat[7] = 0;
			deltaStat[i] = cpustat[i] - cpustat0[i];
			deltaStat[7] += deltaStat[i];
		}
		if ( deltaStat[7] != 0 && rcnt != 0 ) {
			cLoad = (double)(deltaStat[7] - (double)deltaStat[3])/(double)deltaStat[7];
			uLoad = (double)deltaStat[0]/(double)deltaStat[7];
			nLoad = (double)deltaStat[1]/(double)deltaStat[7];
			sLoad = (double)deltaStat[2]/(double)deltaStat[7];
			iLoad = (double)deltaStat[3]/(double)deltaStat[7];
			wLoad = (double)deltaStat[4]/(double)deltaStat[7];
			qLoad = (double)deltaStat[5]/(double)deltaStat[7];
			rLoad = (double)deltaStat[6]/(double)deltaStat[7];
			irqLoad = (qLoad + rLoad)/(double)deltaStat[7];
		} else {
			cLoad = 0;
			uLoad = 0;
			nLoad = 0;
			sLoad = 0;
			iLoad = 1;
			wLoad = 0;
			qLoad = 0;
			rLoad = 0;
			irqLoad = 0;
		}
		tcLoad += cLoad;
		acLoad = tcLoad/(double)(acnt);
		cpuroll[cr] = cLoad;
		tcLoadv = 0;	
		for (int j=0; j<corecnt; j++) {
			for (int i=0; i<7; i++) {
				if ( i == 0 ) deltaStatv[j][7] = 0;
				deltaStatv[j][i] = cpustatv[j][i] - cpustatv0[j][i];
				deltaStatv[j][7] += deltaStatv[j][i];
			}
			if ( deltaStatv[j][7] != 0 && rcnt != 0  ) {
				cLoadv[j] = ((double)deltaStatv[j][7] - (double)deltaStatv[j][3])/(double)deltaStatv[j][7];
			}else {
				cLoadv[j] = 0;
			}
			tcLoadv += cLoadv[j];
		}
		tcLoadv /= corecnt;
		iLoadv = 1 - tcLoadv;
		for (int i=0; i<7; i++) {
			cpustat0[i] = cpustat[i];
			cpustat[i] = 0;
		}
		for (int j=0; j<corecnt; j++) {
			for (int i=0; i<7; i++) {
				cpustatv0[j][i] = cpustatv[j][i];
				cpustatv[j][i] = 0;
			}
		}
		if ( netdis != 0 ) {
			rxbits = netstat[0]>=0?netstat[0]*8:rxbitspast;
			rxpp = netstat[1]>=0?netstat[1]:rxpppast;
			deltarxbits = rcnt>0?rxbits - rxbitspast:0;
			deltarxpp = rcnt>0?rxpp - rxpppast:0;
			rxbitspast = rxbits;
			rxpppast = rxpp;
			rxMbps = rcnt>1?(deltarxbits/decvar)/1000000L:0;
			rxpps = rcnt>1?(deltarxpp/decvar):0;
			rxMbpssum += rxMbps;
			rxMbpsavg = acnt>2?rxMbpssum/(double)(acnt-2):0;
			txbits = netstat[8]>=0?netstat[8]*8:txbitspast;
			txpp = netstat[9]>=0?netstat[9]:txpppast;
			deltatxbits = rcnt>1?txbits - txbitspast:0;
			deltatxpp = rcnt>1?txpp - txpppast:0;
			txbitspast = txbits;
			txpppast = txpp;
			txMbps = rcnt>1?(deltatxbits/decvar)/1000000L:0;
			txpps = rcnt>1?(deltatxpp/decvar):0;
			txMbpssum += txMbps;
			txMbpsavg = acnt>2?txMbpssum/(double)(acnt-2):0;
			rxroll[cr] = rxMbps;
			txroll[cr] = txMbps;
			rxrollA = 0;
			txrollA = 0;
		}
		
		diskstatR = 0;
		diskstatW = 0;
		for (int i=0;i<diskcnt;i++ ) {
			deltadiskR[i] = diskstats[i][2] - diskstats0[i][2];
			diskstats0[i][2] = diskstats[i][2];
			deltadiskW[i] = diskstats[i][6] - diskstats0[i][6];
			diskstats0[i][6] = diskstats[i][6];
			if ( rcnt != 0 ) {
				deltadiskR[i] /= secdecs;
				deltadiskR[i] *= (double)SECTSIZE;
				deltadiskW[i] /= secdecs;
				deltadiskW[i] *= (double)SECTSIZE;
				deltadiskR[i] /= (double)(1024*1024);
				deltadiskW[i] /= (double)(1024*1024);
				if ( scroll == 49 ) printf("%s: \t\tR: %-7.2f \tW: %-7.2f\n", disk[i], deltadiskR[i], deltadiskW[i]);
			}else {
				deltadiskR[i] = (double)0;
				deltadiskW[i] = (double)0;
			}
			diskstatR += deltadiskR[i];
			diskstatW += deltadiskW[i];
		}
		diskstatRsum += diskstatR;
		diskstatRa = acnt>2?diskstatRsum/(double)(acnt-2):0;
		diskstatWsum += diskstatW;
		diskstatWa = acnt>2?diskstatWsum/(double)(acnt-2):0;
		diskrollR[cr] = diskstatR;
		diskrollW[cr] = diskstatW;
		cpurollA = 0;
		diskrollRa = 0;
		diskrollWa = 0;
		tt=rcnt<rint((60/(secdecs)))?cr+1:rint(60/(secdecs));
		nt=rcnt<rint((60/(secdecs)))?acnt-2:rint(60/(secdecs));
		for (int i= 0; i < tt; i++) {
			cpurollA += cpuroll[i];
			if ( netdis != 0 ) rxrollA += rxroll[i];
			if ( netdis != 0 ) txrollA += txroll[i];
			diskrollRa += diskrollR[i];
			diskrollWa += diskrollW[i];
		}
		cpurollA /= (double)tt;
		if ( netdis != 0 ) rxrollA = acnt>2?rxrollA/(double)nt:0;
		if ( netdis != 0 ) txrollA = acnt>2?txrollA/(double)nt:0;
		diskrollRa = acnt>2?diskrollRa/(double)nt:0;
		diskrollWa = acnt>2?diskrollWa/(double)nt:0;
		cr++;
		if ( cr == rint(60/(secdecs)) ) cr = 0;
		free(linebacker);
		exetime = clock();
		cpuspin = ((double)exetime - (double)exetime0)/(double)CLOCKS_PER_SEC;
		cpuspins = fmod(cpuspin,60);
		cpuspinm = (int)cpuspin/60;
		pcpu = cpuspin / cntsum * 100 / corecnt;
		if ( redis != 0 && scroll != 0 ) {
			if ( xcnt != 0 && xcnt % redis == 0 ) print_header();
		}
		if ( scroll < 47 ) print_stats();
		rcnt++;
		acnt++;
		xcnt++;
		
	}
	sig = SIGQUIT;
	return 0;
}
void zeroCounters(void)
{
	for ( int j=0;j<diskcnt;j++) {
		for (int i=0; i<7; i++) {
			diskstats[j][i] = diskstats0[j][i] = 0;
		}
		deltadiskR[j] = 0;
		deltadiskW[j] = 0;
	}
	diskstatR = 0;
	diskstatRsum = 0;
	diskstatRa = 0;
	diskstatW = 0;
	diskstatWsum = 0;
	diskstatWa = 0;
	diskrollRa = 0;
	diskrollWa = 0;
	for (int i=0; i<8; i++) {
		cpustat[i] = cpustat0[i] = deltaStat[i] = 0;
		for (int j=0; j<corecnt; j++) {
			cpustatv[j][i] = cpustatv0[j][i] = deltaStatv[j][i] = 0;
		}
	}
	for (int i=0; i<60000; i++) {
		cpuroll[i] = 0;
		diskrollR[i] = diskrollW[i] = 0;
		if ( netdis != 0 ) rxroll[i] = txroll[i] = 0;
	}
	tcLoad=0;
	if ( rcnt != 0 ) {
		clock_gettime(CLOCK_MONOTONIC, &tstart);
		ttstart = (double)tstart.tv_sec + NSECS2SECS*tstart.tv_nsec;
	}
	slat = 0;
	acnt=1;
	xcnt=0;
	rcnt=0;
	cnt=0;
	idur=idurv;
	cpurollA=0;
	cr=0;
	if ( netdis != 0 ) {
		for (int i=0; i<10; i++) {
			netstat[i] = 0;
		}
		rxrollA = 0;
		txrollA = 0;
		tcLoad=0;
		rxbits = 0;
		deltarxbits = 0;
		rxbitspast = 0;
		rxMbps = 0;
		rxMbpssum = 0;
		rxMbpsavg = 0;
		txbits = 0;
		deltatxbits = 0;
		txbitspast = 0;
		txMbps = 0;
		txMbpssum = 0;
		txMbpsavg = 0;
		rxpps = 0;
		txpps = 0;
		rxpppast = 0;
		txpppast = 0;
		deltarxpp = 0;
		deltatxpp = 0;
	}
	return;
}
void print_stats(void)
{
	if ( idurs >= 59.9 && scroll != 2 ) check_60(idurs);
	if(actlog <= 1) {
		if ( colour != 0 ) printf (LEFTSTATS);
			if ( secdecs >= 0.1 ) printf ("%-10i%i:%05.2f %4.1f  %03i:%02i:%05.2f", acnt,cpuspinm,cpuspins,pcpu,idurh,idurm,idurs);
			if ( secdecs < 0.1 ) printf ("%-10i%i:%05.2f %4.1f  %02i:%02i:%06.3f", acnt,cpuspinm,cpuspins,pcpu,idurh,idurm,idurs);
			if ( colour == 1 ) freq>=atof(maxfreq)?printf (ILOADHI):printf (ILOADLOW);
			if ( colour == 2 ) freq>=atof(maxfreq)?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
			printf ("   %5.0f MHz  ", freq);
			if ( colour == 1 ) ctemp>=TEMPTH?printf (ILOADHI):printf (ILOADLOW);
			if ( colour == 2 ) ctemp>=TEMPTH?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
			printf ("%-5.1f ", ctemp);
			if ( colour == 2 ) ctemp>=TEMPTH?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
			printf ("%-5.1f  ", ftemp);
			if ( colour == 1 ) loadavg[0]>=4*loadavg[1]?printf (ILOADHI):printf (ILOADLOW);
			if ( colour == 2 ) loadavg[0]>=4*loadavg[1]?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
			printf ("%-5.2f ", loadavg[0]);
			if ( colour == 1 ) loadavg[1]>=2*loadavg[2]?printf (ILOADHI):printf (ILOADLOW);
			if ( colour == 2 ) loadavg[1]>=2*loadavg[2]?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
			printf ("%-5.2f ", loadavg[1]);
			if ( colour == 1 ) loadavg[2]>=4?printf (ILOADHI):printf (ILOADLOW);
			if ( colour == 2 ) loadavg[2]>=4?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
			printf ("%-5.2f ",loadavg[2]);
			if ( colour == 1 && verb <= 1 ) iLoad==1.00?printf (ILOADHI):printf (ILOADLOW);
			if ( colour == 2 && verb <= 1 ) iLoad==1.00?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
			if ( colour == 1 && verb >= 2 ) iLoadv==1.00?printf (ILOADHI):printf (ILOADLOW);
			if ( colour == 2 && verb >= 2 ) iLoadv==1.00?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
			if ( verb <= 1 ) printf ("%-6.2f", iLoad);
			if ( verb >= 2 ) printf ("%-6.2f", iLoadv);
			if ( colour == 1 && verb <= 1  ) cLoad>=.50?printf (ILOADHI):printf (ILOADLOW);
			if ( colour == 2 && verb <= 1  ) cLoad>=.50?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
			if ( colour == 1 && verb >= 2  ) tcLoadv>=.50?printf (ILOADHI):printf (ILOADLOW);
			if ( colour == 2 && verb >= 2  ) tcLoadv>=.50?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
			if ( verb <= 1 ) printf ("%-6.2f", cLoad);
			if ( verb >= 2 ) printf ("%-6.2f", tcLoadv);
			if ( colour == 1 ) acLoad>=.25?printf (ILOADHI):printf (ILOADLOW);
			if ( colour == 2 ) acLoad>=.25?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
			printf ("%-6.2f", acLoad);
			if ( colour == 1 ) cpurollA>=.25?printf (ILOADHI):printf (ILOADLOW);
			if ( colour == 2 ) cpurollA>=.25?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
			printf ("%-6.2f", cpurollA);
			if ( verb == 0 ) {
				if ( colour == 1 ) sLoad>=.25?printf (ILOADHI):printf (ILOADLOW);
				if ( colour == 2 ) sLoad>=.25?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
				printf ("%-6.2f", sLoad);
				if ( colour == 1 ) uLoad>=.25?printf (ILOADHI):printf (ILOADLOW);
				if ( colour == 2 ) uLoad>=.25?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
				printf ("%-6.2f", uLoad);
				if ( colour == 1 ) nLoad>=.25?printf (ILOADHI):printf (ILOADLOW);
				if ( colour == 2 ) nLoad>=.25?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
				printf ("%-6.2f", nLoad);
				if ( colour == 1 ) wLoad>=.25?printf (ILOADHI):printf (ILOADLOW);
				if ( colour == 2 ) wLoad>=.25?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
				printf ("%-6.2f", wLoad);
				if ( colour == 1 ) irqLoad>=.25?printf (ILOADHI):printf (ILOADLOW);
				if ( colour == 2 ) irqLoad>=.25?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
				printf ("%-6.2f", irqLoad);
			}
			if ( verb == 2 ) {
				for ( int i=0; i<corecnt; i++ ) {
					if ( colour == 1 ) cLoadv[i]>=.50?printf (ILOADHI):printf (ILOADLOW);
					if ( colour == 2 ) cLoadv[i]>=.50?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
					printf ("%-6.2f", cLoadv[i]);
				}
				if ( colour == 1 ) printf (ILOADLOW);
				if ( colour == 2 ) printf ("\033[0;%im", rng(34,38,1));
				printf ("%04i", procs);
				if ( colour == 2 ) printf ("\033[0;%im", rng(34,38,1));
				printf ("/");
				if ( colour == 2 ) printf ("\033[0;%im", rng(34,38,1));
				printf ("%-3i", procsrun);
				if ( colour == 2 ) printf ("\033[0;%im", rng(34,38,1));
				printf ("%-6i", threads);
				printf ("\n");
			}
			if ( verb == 3 ) {
				if ( colour == 1 ) printf (ILOADLOW);
				if ( colour == 2 ) printf ("\033[0;%im", rng(34,38,1));
				printf (" %-7.2f", diskstatR);
				if ( colour == 2 ) printf ("\033[0;%im", rng(34,38,1));
				printf ("%-7.2f", diskstatRa);
				if ( colour == 2 ) printf ("\033[0;%im", rng(34,38,1));	
				printf ("%-7.2f", diskrollRa);
				if ( colour == 2 ) printf ("\033[0;%im", rng(34,38,1));
				printf ("%-7.2f", diskstatW);
				if ( colour == 2 ) printf ("\033[0;%im", rng(34,38,1));
				printf ("%-7.2f", diskstatWa);
				if ( colour == 2 ) printf ("\033[0;%im", rng(34,38,1));
				printf ("%-8.2f", diskrollWa);
			}
			if ( netdis != 0 && verb == 1 ) {
				if ( colour == 1 ) rxMbps>=(double)(BWCE*BANDWIDTH)?printf (ILOADHI):printf (ILOADLOW);
				if ( colour == 2 ) rxMbps>=(double)(BWCE*BANDWIDTH)?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
				printf ("%-8.2f", rxMbps);
				if ( colour == 1 ) rxMbpsavg>=(double)(BWCE*BANDWIDTH)?printf (ILOADHI):printf (ILOADLOW);
				if ( colour == 2 ) rxMbpsavg>=(double)(BWCE*BANDWIDTH)?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
				printf ("%-8.2f", rxMbpsavg);
				if ( colour == 1 ) rxrollA>=(double)(BWCE*BANDWIDTH)?printf (ILOADHI):printf (ILOADLOW);
				if ( colour == 2 ) rxrollA>=(double)(BWCE*BANDWIDTH)?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
				printf ("%-8.2f", rxrollA);
				if ( colour == 1 ) rxpps>=50000?printf (ILOADHI):printf (ILOADLOW);
				if ( colour == 2 ) rxpps>=50000?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
				if ( rxpps < 100) printf ("%-8.2f", rxpps);
				if ( rxpps >= 100) printf ("%-8.0f", rxpps);
				if ( colour == 1 ) txMbps>=(double)(BWCE*BANDWIDTH)?printf (ILOADHI):printf (ILOADLOW);
				if ( colour == 2 ) txMbps>=(double)(BWCE*BANDWIDTH)?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
				printf ("%-8.2f", txMbps);
				if ( colour == 1 ) txMbpsavg>=(double)(BWCE*BANDWIDTH)?printf (ILOADHI):printf (ILOADLOW);
				if ( colour == 2 ) txMbpsavg>=(double)(BWCE*BANDWIDTH)?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
				printf ("%-8.2f", txMbpsavg);
				if ( colour == 1 ) txrollA>=(double)(BWCE*BANDWIDTH)?printf (ILOADHI):printf (ILOADLOW);
				if ( colour == 2 ) txrollA>=(double)(BWCE*BANDWIDTH)?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
				printf ("%-8.2f", txrollA);
				if ( colour == 1 ) txpps>=50000?printf (ILOADHI):printf (ILOADLOW);
				if ( colour == 2 ) txpps>=50000?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
				if ( rxpps < 100) printf ("%-8.2f\n", txpps);
				if ( rxpps >= 100) printf ("%-8.0f\n", txpps);
			}
			if ( verb == 0 ) {
				if ( colour == 1 ) printf (ILOADLOW);
				if ( colour == 2 ) printf ("\033[0;%im", rng(34,38,1));
				printf ("%04i", procs);
				if ( colour == 2 ) printf ("\033[0;%im", rng(34,38,1));
				printf ("/");
				if ( colour == 2 ) printf ("\033[0;%im", rng(34,38,1));
				printf ("%-3i", procsrun);
				if ( colour == 2 ) printf ("\033[0;%im", rng(34,38,1));
				printf ("%-6i", threads);
			}
			if ( verb == 0 || verb == 3 ) {
				if ( colour == 1 ) (double)memstat[1]/(double)memstat[0]<.25?printf (ILOADHI):printf (ILOADLOW);
				if ( colour == 2 ) (double)memstat[1]/(double)memstat[0]<.25?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
				printf ("%-6.2f", (double)memstat[1]/(double)memstat[0]);
				if ( colour == 1 ) (double)memstat[2]/(double)memstat[0]<.33?printf (ILOADHI):printf (ILOADLOW);
				if ( colour == 2 ) (double)memstat[2]/(double)memstat[0]<.33?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
				printf ("%-6.2f", (double)memstat[2]/(double)memstat[0]);
				if ( colour == 1 ) printf (ILOADLOW);
				if ( colour == 2 ) printf ("\033[0;%im", rng(34,38,1));
				if ( colour == 1 ) memstat[3]-memstat[4]>0?printf (ILOADHI):printf (ILOADLOW);
				if ( colour == 2 ) memstat[3]-memstat[4]>0?printf ("\033[1;%im", rng(37,38,1)):printf ("\033[0;%im", rng(34,38,1));
				if ( scroll == 13 ) printf ("%-6.2f", (double)(memstat[3]-memstat[4])/(double)memstat[3]);
				if ( scroll != 13 ) printf ("%-6.2f\n", (double)(memstat[3]-memstat[4])/(double)memstat[3]);
				if ( scroll == 13 ) printf ("%7.4f% 7.4f %2i\n", ((double)exptime-(double)cnttime)/NSECS,secdecs, slot);
			}
			if ( colour != 0 ) printf (ILOADLOW);
	}
	if(actlog >=1) {
		fprintf (fo,"%i %i:%.2f %.1f %02i:%02i:%06.3f ",acnt,cpuspinm,cpuspins,pcpu,idurh,idurm,idurs);
		fprintf (fo,"%-4.0f %-4.1f %-4.2f %-4.2f %-4.2f ",freq,ctemp,loadavg[0],loadavg[1],loadavg[2]);
		fprintf (fo,"%-4.2f ", iLoad);
		fprintf (fo,"%-4.2f ", iLoadv);
		fprintf (fo,"%-4.2f ", cLoad);
		fprintf (fo,"%-4.2f ", tcLoadv);
		fprintf (fo,"%-4.2f ", acLoad);
		fprintf (fo,"%-4.2f ", cpurollA);
		fprintf (fo,"%-4.2f ", sLoad);
		fprintf (fo,"%-4.2f ", uLoad);
		fprintf (fo,"%-4.2f ", nLoad);
		fprintf (fo,"%-4.2f ", wLoad);
		fprintf (fo,"%-4.2f ", irqLoad);
		for ( int i=0; i<corecnt; i++ ) {
			fprintf (fo,"%-4.2f ", cLoadv[i]);
		}
		if ( netdis != 0 ) {
			fprintf (fo, "%-4.2f ", rxMbps);
			fprintf (fo, "%-4.2f ", rxMbpsavg);
			fprintf (fo, "%-4.2f ", rxrollA);
			fprintf (fo, "%i ", (int)rint(rxpps));
			fprintf (fo, "%-4.2f ", txMbps);
			fprintf (fo, "%-4.2f ", txMbpsavg);
			fprintf (fo, "%-4.2f ", txrollA);
			fprintf (fo, "%i ", (int)rint(txpps));
		}
		fprintf (fo,"%i/%i %i ", procs,procsrun,threads);
		fprintf (fo,"%-4.2f ", (double)memstat[1]/(double)memstat[0]);
		fprintf (fo,"%-4.2f\n", (double)memstat[2]/(double)memstat[0]);
	}
	return;
}
void print_help(void)
{
	if(actlog <= 1) {
		if ( colour != 0 ) printf(HEADER);
		printf ("\ncpu statistics monitor________________________________________________________________________________________________________________________________________________\n\n");
		if ( colour != 0 ) printf(ILOADLOW);
		printf ("  system.type:\t %s\n", sysid);
		printf ("  hostname:\t %s\n", uname);
		printf ("  o/s.version:\t %s\n", version);
		printf ("  cpu.model:\t%s", model[1]);
		printf ("  cpu.cores:\t %i [%s]\n", corecnt, cpuol);
		printf ("  min.freq:\t %.2f MHz\n  max.freq:\t %.2f MHz\n  governor:\t %9s", atof(minfreq), atof(maxfreq), freqgov);
		printf ("  sys.memory:\t %i kB\n", memstat[0]);
		printf ("  sys.disks:\t %i ", diskcnt);
		for (int i=0;i<diskcnt;i++) {
			printf ("[%s]", disk[i]);
		}
		printf ("\n");
		if ( netdis != 0 ) printf ("  net.device:\t %s \n", dev);
		print_current_time_with_ms();
		printf ("  start.time:\t %s\n", xy_string);
		if ( idurds == 0) printf ("  exp.run.time:\t %02i:%02i:%05.2f\n", idurhs,idurms,idurss);
		if ( idurds != 0) printf ("  exp.run.time:\t %02id:%02i:%02i:%05.2f\n", idurds,idurhs,idurms,idurss);
		printf ("  interval:\t %-7.3f\n", secdecs);
		printf ("  samples/s:\t %.2f\n", (double)1/(secdecs));
		printf ("  exp.samples:\t %.2f\n", samples!=0?samples:rint(idurv/(secdecs)));
		if ( rcnt != 0 ) printf ("  current time:\t %s\n", ty_string);
		if (actlog >= 1) printf ("  logging to:\t %s\n", outputfile);
		printf ("\n");
		if ( colour != 0 ) printf(LEFTSTATS);
		printf("  RUNTIME OPTIONS\n");
		if ( scroll == 1 || rcnt == 0 ) printf("  <h>: show runtime help\n");
		printf("  <c>: switch stdout colour\n");
		printf("  <q>: quit cpusm\n");
		printf("  <s>: display interim cpusm summary statistics \n");
		if ( redis == 0 ) printf("  <x>: enable periodic header printing \n");
		if ( redis != 0 ) printf("  <x>: disable periodic header printing \n");
		printf("  <z>: restart cpusm\n");
		printf("  </>: switch output mode [fixed-line/scrolling]\n");	
		printf("  <.>: display column headings\n");
		printf("  <,>: enable/diable stdout [file logging-mode]]\n");
		printf("  <SPACE>: switch display mode [extended-cpu/virtual-cpu/network-interface]\n\n");
		if ( colour != 0 ) printf(ILOADHI);	    
		printf ("  %s\n\n", appstart);
		print_header();
	}
	if(actlog >= 1 && rcnt == 0) {
		fprintf (fo, "\ncpu statistics  monitor________________________________________________________________________________________________________________________________________________________________________\n\n");
		fprintf (fo, "  system type:\t %s\n", sysid);
		fprintf (fo, "  hostname:\t %s\n", uname);
		fprintf (fo, "  o/s version:\t %s\n", version);
		fprintf (fo, "  cpu model:\t%s", model[1]);
		fprintf (fo, "  cpu cores:\t %i [%s]\n", corecnt, cpuol);
		fprintf (fo, "  min.freq:\t %.2f MHz\n  max.freq:\t %.2fMHz\n  governor:\t %9s", atof(minfreq), atof(maxfreq), freqgov);
		fprintf (fo, "  sys.memory:\t %i kB\n", memstat[0]);
		if ( netdis != 0 ) fprintf (fo, "  net.device:\t %s \n", dev);
		print_current_time_with_ms();
		fprintf (fo, "  start time:\t %s\n", xy_string);
		fprintf (fo, "  run time:\t %02id:%02i:%02i:%05.2f\n", idurds,idurhs,idurms,idurss);
		fprintf (fo, "  interval:\t %-7.3f\n", secdecs);
		fprintf (fo, "  samples/s:\t %.2f\n", (double)1/(secdecs));
		fprintf (fo, "  exp.samples:\t %.2f\n", idurv/(secdecs));
		fprintf (fo, "  logging to:\t %s\n\n", outputfile);
		fprintf (fo, "  %s\n\n", appstart);
		print_logheader();
	}
	return;
}
void print_app_usage(void)
{
	printf("\ncpusm -h -c[n] -d[n] -k[n] -l[n] -f[filename] -n[interface] -p[n] -q[n] -s[n] -t[s.n]|-r[n] -v[n] -x[n] -z[n]\n");
	printf("_______________________________________________________________________________________________________________\n");
	printf("command-line options [* default] \n\n");
	printf("-h: execution assistance \n");
	printf("-c [0,1,2]: stdout display colour \n");
	printf(" * 0: colour off\n");
	printf("   1: colour on\n");
	printf("   2: random colour\n");
	printf("-d [0,s.n]: duration mode \n");
	printf(" * 0: perpetual\n");
	printf("   s.n: duration in seconds [conflicts with -z]\n");
	printf("-k [0,1,2]: procs/threads tracking \n");
	printf("   0: no tracking\n");
	printf(" * 1: compiled tracking\n");
	printf("   2: system-based tracking\n");
	printf("-l [0,1,2]: enable logging \n");
	printf(" * 0: logging to stdout only\n");
	printf("   1: logging to file and stdout\n");
	printf("   2: logging to file only\n");
	printf("-f [filename]: log destination\n");
	printf("-n [interface]: network interface to monitor\n");
	printf("-p [0,1]: interval tracking algorithm\n");
	printf(" * 0: direct time\n");
	printf("   1: static interval\n");
	printf("-q [0,1]: timing quality\n");
	printf(" * 0: dynamically adjusted\n");
	printf("   1: static\n");
	printf("-r [n]: sample rate\n");
	printf("   n: samples/second\n");
	printf(" * 10: 10/s\n");
	printf("   maximum: 200\n");
	printf("-s [0,1]: stdout display\n");
	printf(" * 0: fixed-line output\n");
	printf("   1: scrolling-line output\n");
	printf("-t [s.n]: sample interval\n");
	printf("   s.n: seconds\n");
	printf(" * 0.1: 0.1 seconds\n");
	printf("   minimum: 0.005 seconds\n");
	printf("-v [0,1,2,3]: cpusm mode \n");
	printf(" * 0: cpu.core mode\n");
	printf("   1: network mode [use in conjunction with -n]\n");
	printf("   2: virtual.cpu mode\n");
	printf("   2: system.io mode\n");
	printf("-x [n]: periodic column heading display\n");
	printf("   n: samples/display\n");
	printf(" * 0: off\n");
	printf("   n: display column headings ever <n> samples\n");
	printf("   40: default value for runtime enable\n");
	printf("-z [n]: sample mode \n");
	printf(" * 0: no limit\n");
	printf("   n: total number of samples [conflicts with -d]\n");
	printf("_______________________________________________________________________________________________________________\n");
	printf("runtime options \n\n");
	printf("<h>: show runtime help\n");
	printf("<c>: switch stdout colour\n");
	printf("<q>: quit cpusm\n");
	printf("<s>: enable/disable periodic header printing \n");
	printf("<x>: display cpusm summary statistics \n");
	printf("<z>: restart cpusm\n");
	printf("</>: switch output mode [fixed-line/scrolling]\n");	
	printf("<.>: display column headings\n");
	printf("<,>: enable/diable stdout [file logging-mode]]\n");
	printf("<SPACE>: switch display mode [extended-cpu/virtual-cpu/network-interface]\n\n");

	return;
}
void print_header(void)
{
	size_t vs;
	char *verb0;
	xcnt = 0;
	if ( verb == 0 ) {
		verb0 = strdup("cpu.core.stats");
		vs = sizeof("cpu.core.stats");
	}
	if ( verb == 1 ) {
		verb0 = strdup("network.stats");
		vs = sizeof("network.stats");
	}
	if ( verb == 2 ) {
		verb0 = strdup("virtual.cpu.stats");
		vs = sizeof("virtual.cpu.stats");
	}
	if ( verb == 3 ) {
		verb0 = strdup("system.io.stats");
		vs = sizeof("system.io.stats");
	}
	
	int vss = (int)vs;
	if ( colour != 0 ) printf(HEADER);
	printf ("\n%39s","app.stats_____________________________|");
	printf ("%-*s",vss,verb0);
	
	for ( int i=0;i<(127-vss);i++) {
		printf("%s", "_");
	}
	printf("\n");
	if ( colour != 0 ) printf(ILOADLOW);
	printf ("                                     %-1s\n", " |");
	printf ("                                     %-1s", " |");
	if ( colour != 0 ) printf(LEFTSTATS);
	printf ("%-9s____%-8s_____%-11s_________________________________________________________________________________________\n","core.freq","cpu.temp","load.average");
	if ( colour != 0 ) printf(ILOADLOW);
	printf ("%-10s%-7s %-6s%-8s ", "count","ctime","%cpu","runtime");
	printf ("%3s  |","   ");
	if ( colour != 0 ) printf(LEFTSTATS);
	printf ("   ");
	cpuol[strcspn(cpuol,"\n")] = 0;
	printf ("%s       ", cpuol);
	printf (  "%-4s|%-6s  %-5s %-5s %-5s %-6s%-6s%-6s%-6s","\u00B0C","\u00B0F","1m","5m","15m","idle","cpu","avg","1m");
	if ( verb == 0 ) {
		printf ("%-6s%-6s%-6s%-6s%-6s%-8s%-6s%-6s%-6s%-7s\n\n","sys ","user","nice","ioio","irqs","prcs","nlwp","free","avai","swap");
	}
	if ( netdis != 0 && verb == 1 ) {
		printf ("%-8s%-8s%-8s%-8s%-8s%-8s%-8s%-8s\n\n","rxMs","rxMa","rx1m","rxps","txMs","txMa","tx1m","txps");
	}
	if (verb == 2) {
	   for ( int f=0; f<corecnt; f++ ) {
			printf("%-3s%-3i", "cpu", f);
		}
		printf("%-8s%-6s", "prcs","nlwp");
		printf ("\n\n");
	}
	if (verb == 3) {
		printf(" %-7s%-7s%-7s%-7s%-7s%-7s", "dskR","dsRa","dR1m","dskW","dsWa","dW1m");
		printf(" %-6s%-6s%-7s", "free","avai","swap");
		printf ("\n\n");
	}
	if (scroll == 0) {
		printf ("\n");
	}
	if ( colour != 0 ) printf(DEFCOLOR);
	return;
}
void print_logheader(void)
{
	fprintf (fo,"%-4s %-4s %-4s %-4s ","cntt","ctme","%cpu","time");
		fprintf (fo,"%-4s %-4s %-4s ","freq","temp","load");
		fprintf (fo,"%-4s ", "idle");
		fprintf (fo,"%-4s ", "vidl");
		fprintf (fo,"%-4s ", "mcpu");
		fprintf (fo,"%-4s ", "vcpu");
		fprintf (fo,"%-4s ", "cavg");
		fprintf (fo,"%-4s ", "ca1m");
		fprintf (fo,"%-4s ", "syss");
		fprintf (fo,"%-4s ", "usrr");
		fprintf (fo,"%-4s ", "nice");
		fprintf (fo,"%-4s ", "ioio");
		fprintf (fo,"%-4s ", "irqq");
		for ( int i=0; i<corecnt; i++ ) {
			fprintf (fo,"%-3s%i,", "cpu",i);
		}
		if ( netdis != 0 ) {
			fprintf (fo, "%-4s ", "rxMs");
			fprintf (fo, "%-4s ", "rxMa");
			fprintf (fo, "%-4s ", "rxM1");
			fprintf (fo, "%-4s ", "rxps");
			fprintf (fo, "%-4s ", "txMs");
			fprintf (fo, "%-4s ", "txMa");
			fprintf (fo, "%-4s ", "txM1");
			fprintf (fo, "%-4s ", "txps");
		}
		fprintf (fo,"%-4s %-4s ", "prcs","nlwp");
		fprintf (fo,"%-4s ", "avai");
		fprintf (fo,"%-4s ", "free");
		fprintf (fo,"%-4s\n", "swap");
	return;
}
