#define PPMYD "/tmp/pid.ppmyd"
#define AFFCPU "/sys/devices/system/cpu/online"
#define CPUI "/proc/cpuinfo"
#define VER "/proc/version"
#define HN "/etc/hostname"
#define GOV "/sys/devices/system/cpu/cpufreq/policy0/scaling_governor"
#define SOCT "/sys/class/thermal/thermal_zone0/temp"
#define LDAVG "/proc/loadavg"
#define PSTAT "/proc/stat"
#define NETSTAT "/proc/net/dev"
#define MSTAT "/proc/meminfo"
#define DSTAT "/proc/diskstats"
#define FREQ "lscpu --parse=MINMHZ,MAXMHZ | egrep -v '#'"
#define ID "uname -s"
#define THREADS "ps -A H | egrep -c ."
#define PROCS "ps -A | egrep -c ."
#define DEFDEV "ip route list 0.0.0.0/0 | egrep -o 'dev [[:graph:]]*' | sed -e 's/dev //'"
#define DEFDSK "mount | egrep -o '^/dev/[[:graph:]]*' | sed -e 's/.dev.//'"
#define CZERO "\033[0;30m"
#define CONE "\033[0;31m"
#define CTWO "\033[0;32m"
#define CTHREE "\033[0;33m"
#define CFOUR "\033[0;34m"
#define CFIVE "\033[0;35m"
#define CSIX "\033[0;36m"
#define CSEVEN "\033[0;37m"
#define CEIGHT "\033[0;38m"
#define CNINE "\033[0;39m"
#define ILOADLOW "\033[0;37m"
#define ILOADHI "\033[1;37m"
#define CLOADLOW "\033[0;36m"
#define CLOADHI "  \033[1;36m"
#define LEFTSTATS "\033[1;34m"
#define DEFCOLOR "\033[0;m"
#define HEADER "\033[0;7m"
#define HSLEEP 11
#define VARINT 2
#define ENAINT 0
#define NSECS2SECS 1.0e-9
#define NSECS 1000000000L
#define USECS 1000000L
#define BANDWIDTH 1000
#define BWCE 0.75
#define RATEMAX 200
#define INTMIN 0.005
#define SECTSIZE 512
#define TEMPTH 70

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <dirent.h>
#include <locale.h>
#include <signal.h>
#include <pthread.h>
#include <sys/ptrace.h>
#include <inttypes.h>
#include <termios.h>  
